from itertools import *
import copy
import numpy as np
from scipy.spatial.transform import Rotation as R
from atomic_masses import *
from conformers import *
      

class Input_Gen():
    def __init__(self,filename):
        self.filename=filename
    
    def read_input(self):
        lines=[]
        coords=[]
        connectivity=[]
        dihedrals=[]
        output_template=[]
        with open(self.filename,'rt') as file:
            for line in file:
                lines.append(line.rstrip('\n'))
        i=0
        while i<len(lines) and lines[i]!='':
            i+=1
        output_template=lines[0:i-1]+[lines[i-1].replace('=modredundant','')]+['','Title','']+[lines[i+3]]+['Coordinates']
        self.title=lines[i+1].strip()
        
        i=i+4
        while i<len(lines) and lines[i]!='':
            coords.append(lines[i].split())
            i+=1
        self.coords=coords
        
        i=i+1
        while i<len(lines) and lines[i]!='':
            connectivity.append(lines[i].split())
            i+=1
        self.connectivity=connectivity
        
        i=1
        while i<len(lines) and lines[len(lines)-i][0:2]!='D ':
            output_template.extend(lines[len(lines)-i])
            i+=1

        while i<len(lines) and lines[len(lines)-i]!='':
            dihedrals.append(lines[len(lines)-i].split())
            i+=1
        
        self.output_template=output_template
        self.dihedrals=dihedrals
        print('Dihedrals read')
        
        
    def dihedral_sorter(self):
        
        self.substituents=[]
        for d_angle in self.dihedrals:
            sub_graph, sub_cycles, sub_cycle_pat, reference=self.dfs_graph_search(int(d_angle[3]),int(d_angle[2]))
            print(sub_cycle_pat)
            self.substituents.append([d_angle,sub_graph,sub_cycles,sub_cycle_pat,reference])        
        
        
    def dfs_graph_search(self,point,parent):
        N=len(self.coords)+1
        graph=[[] for i in range(N)]
        cycles=[[] for i in range(N)]
        cycle_pat=[[] for i in range(N)]
            
            
            
        def dfs_cycle(u,p,color:list,mark:list,par:list):
            nonlocal cyclenumber
            reference.append(u)
            if color[u]==2:
                return
            if color[u]==1:
                cyclenumber+=1
                cur=p
                mark[cur].append(cyclenumber)
                
                while cur != u:
                    cur=par[cur]
                    mark[cur].append(cyclenumber)
                return
            par[u]=p
            color[u]=1
            if len(self.connectivity[u-1])>2:
                graph[u].extend([[int(self.connectivity[u-1][i]),float(self.connectivity[u-1][i+1])] for i in range(len(self.connectivity[u-1])) if i % 2 == 1])
            
            for sub in graph[u]:
                if [u,sub[1]] not in graph[sub[0]]:
                    graph[sub[0]].append([u,sub[1]])
            
            for v in graph[u]:
                if v[0]==par[u]:
                    continue
                dfs_cycle(v[0],u,color,mark,par)
            color[u]=2
            
        def printCycles(mark:list):
            for i in range(1,N):
                if len(mark[i])>0:
                    for elem in mark[i]:
                        cycles[elem].append(i)
            for i in range(len(cycles)):
                if len(cycles[i])>0:
                    cycle_pat[i]=[[connect[1],self.coords[cycles[i][elem]-1][0]] for elem in range(len(cycles[i])) for connect in graph[cycles[i][elem]] if cycles[i][elem-1] in connect]
                
            for i in range(1,cyclenumber+1):
                print("Cycle Number %d:" % i, end = " ") 
                for x in cycles[i]:
                    print(x, end = " ") 
                print() 
            
        color=[0]*N
        par=[0]*N
        mark = [[] for i in range(N)]
        reference=[]
        cyclenumber=0
        dfs_cycle(point,parent,color,mark,par)
        printCycles(mark)
        return graph, cycles, cycle_pat, reference
            
            

                
    
    def write_ouput(self):
        def sort_sub(s):
            return len(s[4])
        
        def rot_coords(array,rot_axis,origin,reference,degrees):
            rotated_coords=[]
            rot_radians=np.radians(degrees)
            rotation_vector=rot_radians*rot_axis
            rotation=R.from_rotvec(rotation_vector)
            for i in range(len(array)):
                if i+1 in reference:
                    to_rotate=array[i]-origin
                    rotated_coords.append(origin+np.array(rotation.apply(to_rotate)))
                else:
                    rotated_coords.append(array[i])
            return rotated_coords
        
        def midpoint(p1,p2):
            return (p1+p2)/2
        
        def plane_from_points(p1,p2,p3):
            normal=np.cross(p3-p1,p2-p1)
            d=np.dot(normal,p3)
            return normal,d
        
        def get_COM(array,weights,reference):
            com=np.array([0,0,0])
            total_weight=0
            for point in reference:
                total_weight+=weights[point-1]
                com=com+weights[point-1]*array[point-1]
            return com/total_weight
        
        def signed_dist_to_plane(normal,point,arbitrary_point):
            return np.dot((point-arbitrary_point),normal)
        
        def closest_sph_line_int(sph_o,r,p1,p2_d):
            a=(p2_d[0]-p1[0])**2+(p2_d[1]-p1[1])**2+(p2_d[2]-p1[2])**2
            b=2*((p2_d[0]-p1[0])*(p1[0]-sph_o[0])+(p2_d[1]-p1[1])*(p1[1]-sph_o[1])+(p2_d[2]-p1[2])*(p1[2]-sph_o[2]))
            c=sph_o[0]**2+sph_o[1]**2+sph_o[2]**2+p1[0]**2+p1[1]**2+p1[2]**2-2*(sph_o[0]*p1[0]+sph_o[1]*p1[1]+sph_o[2]*p1[2])-r**2
            rad=b**2-4*a*c
            if rad<0:
                raise NameError('Desired angle outside of common bond distance')
            elif rad==0:
                sol=p1-(b/(2*a))*(p2_d-p1)
                return sol-p2_d
            else:
                sol1=p1+((-b+np.sqrt(rad))/(2*a))*(p2_d-p1)
                sol2=p1+((+b+np.sqrt(rad))/(2*a))*(p2_d-p1)
                if np.linalg.norm(sol1-p2_d)>=np.linalg.norm(sol2-p2_d):
                    return sol2-p2_d
                else:
                    return sol1-p2_d
            
        self.substituents.sort(reverse=True,key=sort_sub)
        rotations=[]
        cycles=[]
        cycle_pats=[]
        cycle_confs=[]
        for i in range(len(self.substituents)):
            for j in range(len(self.substituents[i][2])):
                if all(set(self.substituents[i][2][j])!=set(elem) for elem in cycles) and len(self.substituents[i][2][j])>0 and str(self.substituents[i][3][j]) in ring_dict:
                    cycles.append(self.substituents[i][2][j])
                    cycle_pats.append(self.substituents[i][3][j])
                    cycle_confs.append([i for i in range(len(ring_dict[str(self.substituents[i][3][j])]))])
            list_of_other_rings=[cycle for cycle in cycles if cycle not in self.substituents[i:][2]]
            if any(set(self.substituents[i][2][j])==set(elem) for elem in list_of_other_rings for j in range(len(self.substituents[i][2]))):
                rotations.append(list(dict.fromkeys([(360/round(float(self.substituents[i][0][6])))*n for n in range(round(float(self.substituents[i][0][6]))+1)[1:]])))
            else:
                sub_rot=[]
                sub_rot.extend([(360/round(float(self.substituents[i][0][6])))*n for n in range(round(float(self.substituents[i][0][6]))+1)[1:]])
                sub_rot.extend([(360/round(float(self.substituents[i][0][6])))*n+180 for n in range(round(float(self.substituents[i][0][6]))+1)[1:]])
                rotations.append(list(dict.fromkeys(sub_rot)))
        total_conformations=list(product(product(*rotations),product(*cycle_confs)))
        coordinate_array=[np.array([float(coordinates) for coordinates in atom[1:]])for atom in self.coords]
        weights=[mass_dictionary[element[0]] for element in self.coords]
#        print(coordinate_array)
#        print(weights)
        for conformation in total_conformations:
            print(conformation)
            conf_coord_array=copy.deepcopy(coordinate_array)
            for i in range(len(conformation[0])):
                rot_axis=conf_coord_array[int(self.substituents[i][0][2])-1]-conf_coord_array[int(self.substituents[i][0][3])-1]
                norm_rot_axis=rot_axis/np.linalg.norm(rot_axis)
                print(int(self.substituents[i][0][3])-1)
                print(int(self.substituents[i][0][2])-1)
                conf_coord_array=rot_coords(conf_coord_array,norm_rot_axis,conf_coord_array[int(self.substituents[i][0][3])-1],self.substituents[i][4],conformation[0][i])
            for j in range(len(conformation[1])):
                cycle_COM=get_COM(conf_coord_array,weights,cycles[j])
                
                z_vec,d=plane_from_points(cycle_COM,midpoint(conf_coord_array[cycles[j][0]-1],conf_coord_array[cycles[j][1]-1]),midpoint(conf_coord_array[cycles[j][0]-1],conf_coord_array[cycles[j][-1]-1]))
                z_vec_normal=z_vec/np.linalg.norm(z_vec)
                for k in range(len(cycles[j])):
                    ring_subs_k=[]
                    sub_r=[]
                    for ring_subs in self.connectivity[cycles[j][k]-1][1::2]:
                        if int(ring_subs) not in cycles[j]:
                            ring_subs_k.append(int(ring_subs))
                            sub_r.append(np.linalg.norm(conf_coord_array[int(ring_subs)-1]-conf_coord_array[cycles[j][k]-1]))
                            
                    point_dist=signed_dist_to_plane(z_vec_normal,conf_coord_array[cycles[j][k]-1],cycle_COM)
                    zk=0
                    N=len(cycles[j])
                    if N%2==1:
                        for m in range(2,int((N-1)/2)+1):
                            zk+=np.sqrt(2/N)*ring_dict[str(cycle_pats[j])][conformation[1][j]]['qm'][m-2]*np.cos(np.radians(ring_dict[str(cycle_pats[j])][conformation[1][j]]['phim'][m-2])+m*2*np.pi*k/N)
                    else:
                        for m in range(2,int(N/2)):
                            zk+=np.sqrt(2/N)*ring_dict[str(cycle_pats[j])][conformation[1][j]]['qm'][m-2]*np.cos(np.radians(ring_dict[str(cycle_pats[j])][conformation[1][j]]['phim'][m-2])+m*2*np.pi*k/N)
                        zk+=np.sqrt(1/N)*ring_dict[str(cycle_pats[j])][conformation[1][j]]['qm'][int(N/2)-2]*(-1)**k
                    #print(ring_subs_k)
                    #print(sub_r)
                    #print(zk)
                    conf_coord_array[cycles[j][k]-1]=conf_coord_array[cycles[j][k]-1]+(zk-point_dist)*z_vec_normal
                    for rs in range(len(ring_subs_k)):
                        rot_ref=self.dfs_graph_search(ring_subs_k[rs],cycles[j][k])[3]#correct
                        rs_bond=conf_coord_array[ring_subs_k[rs]-1]-(conf_coord_array[cycles[j][k]-1]-zk*z_vec_normal)#not checked

                        unit_rsb=rs_bond/np.linalg.norm(rs_bond)
                        rt_axis=np.cross(z_vec_normal,rs_bond)
                        norm_rt=rt_axis/np.linalg.norm(rt_axis)
                        cur_norm_angle=np.degrees(np.arccos(np.dot(z_vec_normal,unit_rsb)))                
                        rotation_angle=ring_dict[str(cycle_pats[j])][conformation[1][j]]['subs'][k][rs]-cur_norm_angle
                        conf_coord_array=rot_coords(conf_coord_array,norm_rt,conf_coord_array[cycles[j][k]-1]-zk*z_vec_normal,rot_ref,rotation_angle)
                     
                        for atom in rot_ref:
                            conf_coord_array[atom-1]=conf_coord_array[atom-1]+closest_sph_line_int(conf_coord_array[cycles[j][k]-1],np.linalg.norm(rs_bond),conf_coord_array[cycles[j][k]-1]-zk*z_vec_normal,conf_coord_array[ring_subs_k[rs]-1])
            
                    

            writable_coords=[' '+element[0]+' '+str(round(co[0],8))+' '+str(round(co[1],8))+' '+str(round(co[2],8)) for (element,co) in zip(self.coords,conf_coord_array)]
            title=str(conformation)
            combination_output=[]
            for line in self.output_template:
                if line=='Title':
                    combination_output.append(title)
                elif line=='Coordinates':
                    combination_output.extend(writable_coords)
                else:
                    combination_output.append(line)
            combination_output.append('')
            with open(title+'.gjf','w') as f:
                f.writelines("%s\n" % line for line in combination_output)
            print(combination_output)
            
            
            

        
        